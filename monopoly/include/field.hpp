#ifndef FIELD_HPP
#define FIELD_HPP

#include <iostream>
#include <string>

#include <QString>
#include <QGraphicsItem>

enum class Field_kind{
    JAIL,
    GO,
    FREE_PARKING,
    GO_TO_JAIL,
    COMMUNITY,
    CHANCE,
    PROPERTY,
    TAX,
    RAILWAY,
    // electro distribution/water supply
    SUPPLY
};
enum class Owner{
    HUMAN, BOT1, BOT2, BOT3, BANK
};

class Field
{
public:

    Field(unsigned field_id, QString name, Field_kind type);
    //for RAILWAY
    Field(unsigned field_id, QString name, int price, int mortgage, QString color, Field_kind type,int rent_lot,
          int rent_two_houses, int rent_three_houses, int rent_four_houses);
    //for SUPPLY
    Field(unsigned field_id, QString name, int price, int mortgage, QString color, Field_kind type,int rent_lot,
          int rent_two_houses);
    //for PROPERTY
    Field(unsigned field_id, QString name, int price, int mortgage, QString color, int rent_lot, int rent_one_house,
          int rent_two_houses, int rent_three_houses, int rent_four_houses, int rent_hotel,
          int house_price, int hotel_price, Field_kind type);



    void build_house();
    void build_hotel();
    void destroy_house();
    void destroy_hotel();
    int get_rent() const;
    void show(std::ostream &out) const;


    void set_mortgage(bool const set) {mortgaged = set;}
    bool is_mortgaged() const {return mortgaged;}

    void set_hotel(const bool hotel) {has_hotel_ = hotel;}
    bool has_hotel() const {return has_hotel_;}

    Owner get_owner() const {return owner_;}
    void change_owner(const Owner o) {owner_ = o;}

    int get_num_of_houses() const {return number_of_houses_;}
    Field_kind get_kind() const {return type_;}
    unsigned get_id() const {return field_id_;}
    int get_price() const {return price_;}
    QString get_name() const {return name_;}
    QString get_color() const {return color_;}
    int get_rent_lot() const {return rent_lot_;}
    int get_rent_one_house() const {return rent_one_house_;}
    int get_rent_two_houses() const {return rent_two_houses_;}
    int get_rent_three_houses() const {return rent_three_houses_;}
    int get_rent_four_houses() const {return rent_four_houses_;}
    int get_rent_hotel() const {return rent_hotel_;}
    int get_house_price() const {return house_price_;}
    int get_hotel_price() const {return hotel_price_;}
    bool get_has_hotel() const {return has_hotel_;}
    int get_mortgage() const {return mortgage_;}

protected:

    unsigned field_id_;
    bool mortgaged = false;
    QString name_;
    int price_;
    int mortgage_;
    QString color_;
    int rent_lot_;
    Owner owner_ = Owner::BANK;

private:

    int rent_one_house_;
    int rent_two_houses_;
    int rent_three_houses_;
    int rent_four_houses_;
    int rent_hotel_;

    int house_price_;
    int hotel_price_;
    int number_of_houses_ = 0;
    bool has_hotel_ = false;
    Field_kind type_;
};


std::ostream &operator<<(std::ostream &out, const Field &field);

#endif // FIELD_HPP
