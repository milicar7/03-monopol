#ifndef ANIMATE_PIXMAP_HPP
#define ANIMATE_PIXMAP_HPP

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>

class Animate_Pixmap : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)

public:
        Animate_Pixmap(QGraphicsPixmapItem * parent);
        QPointF pos() const;
        QPropertyAnimation *animaton() const;
        void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget *);
        QRectF boundingRect() const;

public slots:

        void setPos(QPointF &point);

private:

        QGraphicsPixmapItem *parent_;
        QPropertyAnimation *animation_;

};

#endif // ANIMATE_PIXMAP_HPP
