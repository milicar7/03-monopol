#ifndef ANIMATE_PLAYER_HPP
#define ANIMATE_PLAYER_HPP

#include <QString>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

#include "animate_pixmap.hpp"

class Animate_Player : public QObject
{
    Q_OBJECT

public:
        Animate_Player(QString str, QGraphicsPixmapItem* token_pixmap);
        void draw_grid(QGraphicsScene &scene);
        void animate_advancing(Animate_Pixmap* animator, qint16 adjust);
        void setAdvance(const qint16 &advance);
        qint16 advance() const;
        qint32 position() const;
        void setPosition(const qint32 &position);
        void hide_token();


signals:
        void finished_animation_for_player();
        void finished_back_animation();
        void finished_jail_animation();
public slots:
        void animate_player();
        void go_back_three_spaces();
        void go_to_jail();
private:
        QString str_;
        QGraphicsPixmapItem* token_pixmap_;
        qint32 advance_;
        qint32 x_;
        qint32 y_;
        qint32 position_;
};
#endif // ANIMATE_PLAYER_HPP
