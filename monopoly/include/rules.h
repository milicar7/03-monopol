#ifndef RULES_H
#define RULES_H

#include <QDialog>
#include <QMediaPlayer>
#include <QPushButton>
#include <QKeyEvent>

namespace Ui {
    class Rules;
}

class Rules : public QDialog
{
    Q_OBJECT

public:
    explicit Rules(QWidget *parent = nullptr);
    ~Rules();


    void button_click_sound() const;
    void keyPressEvent(QKeyEvent *event) override;

private slots:
    void button_back_clicked();

private:
    Ui::Rules *ui;
    QPushButton *button_;
    QMediaPlayer *button_click;
};

#endif // RULES_H
