#ifndef PAUSE_H
#define PAUSE_H

#include <QDialog>
#include <QMediaPlayer>

namespace Ui {
class Pause;
}

class Pause : public QDialog
{
    Q_OBJECT

public:
    explicit Pause(QWidget *parent = nullptr);
    ~Pause();

public slots:
    void button_resume_clicked();
    void button_quit_clicked();

private:

    void button_click_sound();

    Ui::Pause *ui;

    QMediaPlayer *button_click;
};

#endif // PAUSE_H
