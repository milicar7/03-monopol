#ifndef WELCOME_HPP
#define WELCOME_HPP

#include <QWidget>
#include <QPalette>
#include <QPainter>
#include <QMediaPlayer>
#include <QDialog>
#include <QColor>
#include <QKeyEvent>

#include "rules.h"

#include <deque>
#include <iostream>

namespace Ui {
class Welcome;
}

class Welcome : public QDialog
{
    Q_OBJECT

public:

    explicit Welcome(QString *name, QString *token, QDialog *parent = nullptr);
    ~Welcome();
    void paintEvent(QPaintEvent *event) override;    
    void button_click_sound() const;
    void keyPressEvent(QKeyEvent *event) override;

public slots:
    void quit_clicked();
    void next_clicked();
    void back_clicked();
    void start_clicked();
    void rules_clicked();

private:
    Ui::Welcome *ui;
    std::deque<QString> tokens;
    QString human_token = ":/images/car.png";
    QString username = "";

    QMediaPlayer *button_click;

    QString *name_;
    QString *token_;
};

#endif // WELCOME_HPP
