#include "../include/bank.hpp"
#include "../include/player.hpp"
#include "../include/bot.hpp"
#include "../include/board.hpp"
#include "../include/field.hpp"
#include "../include/game.hpp"
#include "../include/notification.hpp"
#include "../include/animate_pixmap.hpp"
#include "../include/animate_player.hpp"
#include "../include/keyPressEventFilter.h"
#include "../include/game_over.h"
#include "../include/welcome.hpp"
#include "../include/rectangle_with_text.hpp"
#include "../include/card_property.hpp"

#include <QColor>
#include <QtGui>
#include <QScreen>
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QDesktopWidget>
#include <QGraphicsRectItem>
#include <QTime>
#include <cmath>

//static const unsigned player_count = 4u;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(static_cast<unsigned>(QTime(0,0,0).secsTo(QTime::currentTime())));
    a.setWindowIcon(QIcon(":/images/money_bag_icon.png"));

  //*************************ZA PRIKAZ TABLE****************************************

    QString name = "";
    QString token = "";
    Welcome *wlcm = new Welcome(&name, &token);
    //wlcm->setWindowState(Qt::WindowFullScreen);
    int quit = wlcm->exec();

    // Checks if player wants to quit or he wants to play
    if (quit == QDialog::Rejected)
        return 0;

    KeyPressEventFilter *filter = new KeyPressEventFilter(&a);
    a.installEventFilter(filter);

    /**** FROM HERE STARTS GAME!!!!!!!!!!!!!!!!!!!! ****/

    QGraphicsScene *scene_table= new QGraphicsScene(0, 0, 1900,1000);
   // scene_table->setStyle();
    scene_table->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene_table->setBackgroundBrush(QBrush(QImage(":/images/background.png")));
    Board *b=new Board();
    Game* monopoly = new Game(b,name,token);

//    for (auto n : nesto)
//        std::cout << n.first << " " << *n.second << std::endl;

    //prosledjuje scenu,crta tablu i user deo i stavlja patkicu spremnu za animaciju
    monopoly->initialize_board(scene_table);

    //KAD SE UBACE BOTOVI TREBA SKROZ ZABRANITI DA MOZE DA SE KLIKCE DUGME DOK ONI NE ZAVRSE
    // QObject::connect(monopoly->getBoard()->getCurrent_player(),SIGNAL(finished_animation_for_player()), monopoly->getBoard(), SIGNAL(enable_button_board()) );

    //     QObject::connect(this, SIGNAL(sell_property_and_remove_from_owned_properties(QString)), this, SLOT(remove_property_from_owned_properties(QString)));

    QObject::connect(monopoly,SIGNAL(stepped_on_property()), monopoly, SLOT(ask_about_buying()));
    QObject::connect(monopoly,SIGNAL(stepped_on_tax()), monopoly, SLOT(ask_about_tax()));
    QObject::connect(monopoly, SIGNAL(add_to_owned_properties(QString)), monopoly, SLOT(display_in_owned_properties(QString)));

    //bas u logici obradi odlazak u zatvor
    QObject::connect(monopoly, SIGNAL(stepped_on_go_to_jail()), monopoly, SLOT(tell_about_going_to_jail()));
    //   //animacija odlaska u zatvor

    //uklanja text item sa scene
    QObject::connect(monopoly->getBoard()->getCurrentPlayer(0), SIGNAL(finished_jail_animation()), monopoly, SLOT(remove_tell_about_going_to_jail()));

    QObject::connect(monopoly, SIGNAL(player_owns_jail_card()), monopoly, SLOT(ask_about_using_jail_card()));
    QObject::connect(monopoly, SIGNAL(player_doesnt_own_jail_card()), monopoly, SLOT(pay_to_get_out_of_jail()));

    //TODO: vidi za ovaj slot da ga zamenis slotom rmeove top notif and okay
    QObject::connect(monopoly, SIGNAL(finished_tell_about_not_having_money()), monopoly, SLOT(remove_tell_about_not_having_money())); 

    QObject::connect(monopoly, SIGNAL(do_you_want_to_lift_the_mortgage()), monopoly, SLOT(ask_about_lifting_mortgage()));
    QObject::connect(monopoly, SIGNAL(enough_money_to_lift_the_mortgage()), monopoly, SLOT(remove_notif_about_lifting()));
    //TODO: vidi za ovaj slot da ga zamenis slotom rmeove top notif and okay
    QObject::connect(monopoly, SIGNAL(not_enough_money_to_lift_the_mortgage()), monopoly, SLOT(remove_tell_about_not_having_money()));
    QObject::connect(monopoly, SIGNAL(enough_money_to_lift_the_mortgage()), monopoly, SLOT(ask_about_previously_bought_field()));
    QObject::connect(monopoly, SIGNAL(stepped_on_previously_bought_field()), monopoly, SLOT(ask_about_previously_bought_field()));
    QObject::connect(monopoly, SIGNAL(sell_property_and_remove_from_owned_properties(QString)), monopoly, SLOT(remove_property_from_owned_properties(QString)));
    QObject::connect(monopoly, SIGNAL(display_chance_community_card(Card_Chance_Community*)),monopoly,SLOT(add_chance_community_card_to_the_scene(Card_Chance_Community*)));
    // PAZI NA BOTOVEEEE
    QObject::connect(monopoly, SIGNAL(got_jail_free_card()),monopoly,SLOT(activate_jail_free_card_posetion()));
    QObject::connect(monopoly, SIGNAL(change_money_from_a_card_signal()),monopoly,SLOT(change_budget_rect()));
    QObject::connect(monopoly, SIGNAL(stepped_on_another_players_property()), monopoly, SLOT(pay_to_other_player()));
    QObject::connect(monopoly, SIGNAL(next_player()), monopoly, SLOT(change_current_player()));

    QObject::connect(monopoly, SIGNAL(hocu_move_player_on_the_board()), monopoly, SLOT(move_player_on_the_board()));
    QObject::connect(monopoly, SIGNAL(bots_is_rolling_the_dice()), monopoly->getBoard(), SLOT(animate_dice()));
    QObject::connect(monopoly, SIGNAL(human_wants_to_roll()), monopoly, SLOT(process_player_being_in_jail()));
    QObject::connect(monopoly, SIGNAL(processing_about_jail_finished()), monopoly->getBoard(), SLOT(animate_dice()));
    QObject::connect(monopoly, SIGNAL(processing_about_jail_with_work_finished()), monopoly->getBoard(), SLOT(animate_dice()));

    QObject::connect(monopoly, SIGNAL(processing_about_jail_finished()), monopoly, SLOT(move_player_on_the_board()));
    QObject::connect(monopoly, SIGNAL(processing_about_jail_with_work_finished()), monopoly, SLOT(move_player_on_the_board()));
    QObject::connect(monopoly , SIGNAL(next_player()), monopoly, SLOT(remove_chance_community_card_from_scene()));

    QObject::connect(monopoly , SIGNAL(player_bankrupted()), monopoly, SLOT(return_score()));

    monopoly->init_game();

    QGraphicsView *view_table = new QGraphicsView(scene_table);
    view_table->setAlignment(Qt::AlignCenter);
    view_table->setRenderHint(QPainter::Antialiasing);
    view_table->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    view_table->setDragMode(QGraphicsView::ScrollHandDrag);
    view_table->setWindowTitle("Monopoly");
    view_table->showFullScreen();
    b->view_ = view_table;
 
//    QTimer::singleShot(2500,&view,SLOT(showMaximized()));

    return a.exec();
}
