#include "include/game.hpp"

void Game::set_top_notif_and_okay(QString str)
{
    QGraphicsTextItem *top_notif = new QGraphicsTextItem(str);
    top_notif->setTextWidth(250);
    top_notif->setPos(1640, 575);

    Rectangle_with_text *okay = new Rectangle_with_text("Okay",   Qt::darkGreen,1680, 700,100,30);

    board_->scene_->addItem(top_notif);
    board_->scene_->addItem(okay);

    QObject::connect(okay, SIGNAL(okay_i_dont_have_money_to_buy()), this, SLOT(remove_top_notif_and_okay()));
}
void Game::remove_top_notif_and_okay()
{
    QGraphicsItem *top_notif = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(top_notif);
    delete top_notif;

    QGraphicsItem* okay = board_->view_->itemAt(1680, 700);
    board_->scene_->removeItem(okay);
    delete okay;
}
void Game::change_budget_rect()
{
    Rectangle_with_text *budget_rect = qgraphicsitem_cast<Rectangle_with_text*>(board_->view_->itemAt(1055, 570));
    board_->scene_->removeItem(budget_rect);

    QString s = QString::number(curr_player_->get_budget());
    s.append("€");
    budget_rect->set_text(s);
    board_->scene_->addItem(budget_rect);
}

//sets all the elements player needs to play/follow the game on the scene
void Game::display_user_info(){
    QPen pen(Qt::black);

    board_->scene_->addItem(bot1_about);
    board_->scene_->addItem(bot2_about);
    board_->scene_->addItem(bot3_about);

    QGraphicsLineItem *divide_line = new QGraphicsLineItem(1000,500,1920,500);
    divide_line ->setPen(pen);

    QGraphicsTextItem *current_money_text= new QGraphicsTextItem("Your budget:");
    current_money_text->setPos(1050,550);
    Rectangle_with_text *current_money_box = new Rectangle_with_text("1500€",Qt::darkGreen,1055, 570,100,20);

    Rectangle_with_text *roll_the_dice_button=new Rectangle_with_text("Roll the dice!",Qt::red,1300,570,250,50);

    QGraphicsTextItem *out_of_jail_text= new QGraphicsTextItem("Get out of jail card:");
    out_of_jail_text->setPos(1050,600);
    QGraphicsRectItem *out_of_jail = new QGraphicsRectItem (QRectF(1180,600,20,20));
    out_of_jail->setBrush(Qt::darkRed);

    QGraphicsTextItem *current_properties_text= new QGraphicsTextItem("Properties you currently own :");
    current_properties_text->setPos(1050,630);
    QGraphicsRectItem *current_properties_box = new QGraphicsRectItem (QRectF(1050,650,500,350));
    current_properties_box->setPen(pen);

    QGraphicsRectItem *question_field = new QGraphicsRectItem (QRectF(1600, 570,300,330));
    question_field->setPen(pen);

    Rectangle_with_text *end_turn=new Rectangle_with_text("End turn",Qt::red,1600,930,300,70);

    QObject::connect(roll_the_dice_button, SIGNAL(roll_dice()), this, SIGNAL(human_wants_to_roll()));
    QObject::connect(roll_the_dice_button, SIGNAL(roll_dice()), end_turn, SLOT(enable_button()));

    QObject::connect(this, SIGNAL(enable_button(QString)), roll_the_dice_button, SLOT(enable_button(QString)));

    QObject::connect(end_turn, SIGNAL(end_turn()), this , SIGNAL(next_player()));

    board_->scene_->addItem(divide_line);
    board_->scene_->addItem(current_money_text);
    board_->scene_->addItem(current_money_box);
    board_->scene_->addItem(out_of_jail_text);
    board_->scene_->addItem(out_of_jail);
    board_->scene_->addItem(roll_the_dice_button);
    board_->scene_->addItem(question_field);
    board_->scene_->addItem(end_turn);
    board_->scene_->addItem(current_properties_text);
    board_->scene_->addItem(current_properties_box);
}
