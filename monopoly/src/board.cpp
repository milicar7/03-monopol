#include "../include/board.hpp"

Board::Board()
{

    load();

}
void Board::set_scene(QGraphicsScene *scene) {scene_ = scene;}

Board::~Board()
{
    for (auto c : community_cards_)
        delete c;

    for (auto c : chance_cards_)
        delete c;

    for (auto f : fields_)
        delete f.second;

    for (auto b : buttons_fields_)
            delete b;
    delete scene_;
}

void Board::set_dice_numbers()
{
    auto  rand = QRandomGenerator::global();
    int n1 = rand->bounded(1,7);
    int n2 = rand->bounded(1,7);
    dice1_ = n1;
    dice2_ = n2;
}



Field* Board::get_field_by_id(unsigned field_id) const{

    auto it = fields_.find(field_id);

    return it->second;
}

Card_Chance_Community* Board::take_card(const Card_category c){
    if(c == Card_category::Chance){
        auto card = chance_cards_.front();
        chance_cards_.pop_front();
        return card;
    }
    else{
        auto card = community_cards_.front();
        community_cards_.pop_front();
        return card;
    }
}



Button *Board::get_button_by_name(QString& name) const
{
   auto buttons = get_buttons();
   for (auto b : buttons){
       if(b->getText()==name)
           return b;
   }
}


unsigned Board::get_field_id(const QString &name) const
{
    for (auto f : fields_)
        if (f.second->get_name() == name)
            return f.first;
}
Field* Board::read_field(const QVariantMap &json){

    QString name = json["name"].toString();
    Field_kind kind;
    int rent_lot;
    int rent_two_houses;
    if (name.contains("stanica", Qt::CaseInsensitive) || name.contains("aerodrom", Qt::CaseInsensitive)){
        kind = Field_kind::RAILWAY;
        rent_lot = 25;
        rent_two_houses=50;
        int rent_three_houses=100;
        int rent_four_houses = 200;
        return new Field(json["id"].toInt(),
                        name,
                        json["price"].toInt(),
                        json["mortgage"].toInt(),
                        json["color"].toString(),
                        kind,
                        rent_lot,rent_two_houses,rent_three_houses,rent_four_houses
                        );

 }
    else{
        kind = Field_kind::SUPPLY;
        rent_lot = 100;
        rent_two_houses=200;
        return new Field(json["id"].toInt(),
                        name,
                        json["price"].toInt(),
                        json["mortgage"].toInt(),
                        json["color"].toString(),
                        kind,
                        rent_lot,rent_two_houses
                        );
    }
}

Card_category convert_string_to_card_category(const QString& str)
{
    if(str == "Community") 
        return Card_category::Community;
    else 
        return Card_category::Chance;

}

Card_type convert_string_to_card_type(const QString& str)
{

    if(str == "move") return Card_type::Move;
    else if(str == "get") return Card_type::Get;
    else if(str == "pay") return Card_type::Pay;
    else if(str == "jail") return Card_type::Jail;
    else return Card_type::Jail_free;
}

Card_Chance_Community* Board::readCard_Chance_Community(const QVariantMap &json)
{
        const QString content=json["content"].toString();
        const Card_category cc = convert_string_to_card_category(json["category"].toString());
        QVariantList list_of_types = json["type"].toList();
        std::vector<Card_type> card_types;
        for (QVariantList::const_iterator j = std::begin(list_of_types); j != std::end(list_of_types); j++)
        {
            card_types.push_back(convert_string_to_card_type((*j).toString()));
        }

        return new Card_Chance_Community(content,cc,card_types);
}

Field* Board::read_property(const QVariantMap &json)
{
        return new Field(json["id"].toInt(),
                        json["name"].toString(),
                        json["price"].toInt(),
                        json["mortgage"].toInt(),
                        json["color"].toString(),
                        json["rent_lot"].toInt(),
                        json["rent_one_house"].toInt(),
                        json["rent_two_houses"].toInt(),
                        json["rent_three_houses"].toInt(),
                        json["rent_four_houses"].toInt(),
                        json["rent_hotel"].toInt(),
                        json["house_price"].toInt(),
                        json["hotel_price"].toInt(),
                        Field_kind::PROPERTY);

}

QJsonArray Board::open_and_test_json_file(QFile &file_obj){
    if(!file_obj.exists()){
        std::cout<<"Doesnt exist:  "<< file_obj.fileName().toStdString();
    }
    if(!file_obj.open(QIODevice::ReadOnly)){
        std::cout<<"Failed to open resource: "<< file_obj.fileName().toStdString();
        exit(1);
    }
    QTextStream file_text(&file_obj);
    QString json_string;
    json_string = file_text.readAll();
    file_obj.close();
    QByteArray json_bytes = json_string.toLocal8Bit();

    auto json_doc=QJsonDocument::fromJson(json_bytes);

    if(json_doc.isNull()){
        std::cout<<"Failed to create JSON doc.";
        exit(2);
    }
    if(!json_doc.isArray()){
        std::cout << "JSON doc is not an array.";
        exit(6);
    }
    QJsonArray json_array = json_doc.array();
    if(json_array.isEmpty()){
        std::cout<< "The array is empty";
        exit(7);
    }

    return json_array;
}

QJsonObject Board::test_element(QJsonValue &element){
    if(!element.isObject()){
        std::cout<<"JSON is not an object.";
        exit(3);
    }
    QJsonObject obj = element.toObject();
    if(obj.isEmpty()){
        std::cout<<"JSON object is empty.";
        exit(4);
    }

    return obj;
}


void Board::return_card(Card_Chance_Community* c){
    if(c->getCategory() == Card_category::Chance){
        chance_cards_.push_back(c);
    }
    else{
        community_cards_.push_back(c);
    }

}
void Board::load()
{
    QFile fileProps(":/resources/places.json");
    QJsonArray json_array=open_and_test_json_file(fileProps);
    QJsonValue element;
    QJsonObject obj;
    QVariantMap json_map;
    Field *field;

    for(int i=0; i< json_array.count(); ++i){
        element=json_array.at(i);
        obj = test_element(element);
        json_map = obj.toVariantMap();
        field = read_property(json_map);
        fields_[field->get_id()] = field;
    }


    QFile fileFields(":/resources/special_places.json");
    json_array=open_and_test_json_file(fileFields);
    for(int i=0; i< json_array.count(); ++i){
        element=json_array.at(i);
        obj = test_element(element);
        json_map = obj.toVariantMap();
        field=read_field(json_map);
        fields_[field->get_id()] = field;
    }

    Card_Chance_Community *ccc;
    QFile fileCardsCC(":/resources/community_and_change_cards.json");
    json_array=open_and_test_json_file(fileCardsCC);
    for(int i=0; i< json_array.count(); ++i){
        element=json_array.at(i);
        obj = test_element(element);
        json_map = obj.toVariantMap();
        ccc=readCard_Chance_Community(json_map);

        if(ccc->getCategory()==Card_category::Community)
            community_cards_.push_back(ccc);
        else
            chance_cards_.push_back(ccc);
    }

    std::srand(std::time(0));
    std::random_shuffle(community_cards_.begin(), community_cards_.end());
    std::random_shuffle(chance_cards_.begin(), chance_cards_.end());
    load_special_fields();
}
void Board::load_special_fields()
{
    fields_[0] = new Field(0, "Start", Field_kind::GO);
    fields_[2] = new Field(2, "Community", Field_kind::COMMUNITY);
    fields_[4] = new Field(4, "Pay tax", Field_kind::TAX);
    fields_[7] = new Field(7, "Chance", Field_kind::CHANCE);

    // Be careful, just because you are on this field it doesn't mean you
    // are in jail, you could also be in visit in jail
    fields_[10] = new Field(10, "Jail", Field_kind::JAIL);

    fields_[17] = new Field(17, "Community", Field_kind::COMMUNITY);
    fields_[20] = new Field(20, "Free parking", Field_kind::FREE_PARKING);
    fields_[22] = new Field(22, "Chance", Field_kind::CHANCE);
    fields_[30] = new Field(30, "Go to jail", Field_kind::GO_TO_JAIL);
    fields_[33] = new Field(33, "Community", Field_kind::COMMUNITY);
    fields_[36] = new Field(36, "Chance", Field_kind::CHANCE);
    fields_[38] = new Field(38, "Pay tax", Field_kind::TAX);
}

QGraphicsPixmapItem* Board::rotateCard(QGraphicsPixmapItem *card){

    QPointF center=card->boundingRect().center();
    QTransform transform;
    transform.translate(center.x(),center.y());
    transform.rotate(-45);
    transform.translate(-center.x(),-center.y());
    card->setTransform(transform);
    return card;
}



Animate_Player *Board::getCurrentPlayer(int index) const
{
    if(index>3){
        std::cout<<"Error, we dont have that many bots";
        return nullptr;
    }
    else
        return players_[index];
}




void Board::display_table()
{
    QColor color(204,255,229);
    QBrush brush(color);
    QPen pen(Qt::black);
    //the frame of the board. Parent for all the smaller componenets
    QGraphicsRectItem *rect_outer = new QGraphicsRectItem ();
    rect_outer ->setRect(QRectF(0, 0, OUTER_BOX, OUTER_BOX));
    rect_outer->setBrush(brush);
    rect_outer ->setPen(pen);
    QGraphicsRectItem *rect_inner = new QGraphicsRectItem (QRectF(FIELD_HEIGHT,FIELD_HEIGHT,INNER_BOX,INNER_BOX),rect_outer);

    QPixmap pic_free_parking(":/images/free_parking.png");
    QGraphicsPixmapItem *free_parking= new QGraphicsPixmapItem(pic_free_parking.scaled(FIELD_HEIGHT,FIELD_HEIGHT));

    QPixmap pic_go_to_jail(":/images/go_to_jail.png");
    QGraphicsPixmapItem *go_to_jail= new QGraphicsPixmapItem(pic_go_to_jail.scaled(FIELD_HEIGHT,FIELD_HEIGHT));
    go_to_jail->setOffset(INNER_BOX+FIELD_HEIGHT,0);


    QPixmap pic_jail(":/images/jail.png");
    QGraphicsPixmapItem *jail= new QGraphicsPixmapItem(pic_jail.scaled(FIELD_HEIGHT,FIELD_HEIGHT));
    jail->setOffset(0,INNER_BOX+FIELD_HEIGHT);

    QPixmap pic_start(":/images/go.png");
    QGraphicsPixmapItem *start= new QGraphicsPixmapItem(pic_start.scaled(FIELD_HEIGHT,FIELD_HEIGHT));
    start->setOffset(INNER_BOX+FIELD_HEIGHT,INNER_BOX+FIELD_HEIGHT);

    QGraphicsRectItem *rect_parking = new QGraphicsRectItem (QRectF(0, 0, FIELD_HEIGHT,FIELD_HEIGHT),rect_outer);
    QGraphicsRectItem *rect_go_to_jail = new QGraphicsRectItem (QRectF(INNER_BOX+FIELD_HEIGHT, 0, FIELD_HEIGHT,FIELD_HEIGHT),rect_outer);
    QGraphicsRectItem *rect_jail = new QGraphicsRectItem(QRectF(0,INNER_BOX+FIELD_HEIGHT, FIELD_HEIGHT,FIELD_HEIGHT),rect_outer);
    QGraphicsRectItem *rect_start = new QGraphicsRectItem (QRectF(INNER_BOX+FIELD_HEIGHT,INNER_BOX+FIELD_HEIGHT, FIELD_HEIGHT,FIELD_HEIGHT),rect_outer);

    QPixmap pic_chance_card(":/images/chance_card.png");
    QGraphicsPixmapItem *chance_card= new QGraphicsPixmapItem(pic_chance_card.scaled(CARD_WIDTH,CARD_HEIGHT));
    chance_card->setOffset(CHANCE_CARD_POSITION_X,CHANCE_CARD_POSITION_Y);
    chance_card=rotateCard(chance_card);

    QPixmap pic_community_card(":/images/community_card.png");
    QGraphicsPixmapItem *community_card = new QGraphicsPixmapItem(pic_community_card.scaled(CARD_WIDTH,CARD_HEIGHT));
    community_card->setOffset(COMMUNITY_CARD_POSITION_X,COMMUNITY_CARD_POSITION_Y);
    community_card=rotateCard(community_card);

    QGraphicsRectItem *rect_upper = new QGraphicsRectItem (QRectF(FIELD_HEIGHT, 0, 9 * FIELD_WIDTH, FIELD_HEIGHT), rect_outer);
    QGraphicsRectItem *rect_left = new QGraphicsRectItem (QRectF(0, FIELD_HEIGHT, FIELD_HEIGHT, 9 * FIELD_WIDTH), rect_outer);
    QGraphicsRectItem *rect_right = new QGraphicsRectItem (QRectF(9 * FIELD_WIDTH + FIELD_HEIGHT, FIELD_HEIGHT, FIELD_HEIGHT, 9 * FIELD_WIDTH), rect_outer);
    QGraphicsRectItem *rect_bottom = new QGraphicsRectItem (QRectF(FIELD_HEIGHT, 9 * FIELD_WIDTH + FIELD_HEIGHT, 9 * FIELD_WIDTH, FIELD_HEIGHT), rect_outer);

    Q_UNUSED(rect_inner)
    Q_UNUSED(rect_parking)
    Q_UNUSED(rect_go_to_jail)
    Q_UNUSED(rect_jail)
    Q_UNUSED(rect_start)



    scene_->addItem(rect_outer);
    scene_->addItem(free_parking);
    scene_->addItem(go_to_jail);
    scene_->addItem(jail);
    scene_->addItem(start);
    scene_->addItem(chance_card);
    scene_->addItem(community_card);

    add_special_buttons(rect_bottom, rect_left, rect_upper, rect_right);
    add_buttons(rect_outer);




    for (auto button : buttons_fields_){
        if (button->get_kind()==Field_kind::PROPERTY || button->get_kind()==Field_kind::RAILWAY || button->get_kind()==Field_kind::SUPPLY)
        {
            QObject::connect(button, SIGNAL(show_property_card(Card_Property*)),
                             this, SLOT(show_property_card(Card_Property*)));
            QObject::connect(button, SIGNAL(hide_property_card(Card_Property*)),
                             this, SLOT(hide_property_card(Card_Property*)));
        }
    }
}

void Board::add_special_buttons(QGraphicsRectItem *rect_bottom, QGraphicsRectItem *rect_left, QGraphicsRectItem *rect_upper, QGraphicsRectItem *rect_right)
{
    QPixmap pic_chance(":/images/chance_question_mark.png");
    QGraphicsPixmapItem *chance1 = new QGraphicsPixmapItem(pic_chance.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_bottom);
    chance1->setPos(INNER_BOX + FIELD_HEIGHT - 7 * FIELD_WIDTH, INNER_BOX + FIELD_HEIGHT + COLOR_FIELD);

    QGraphicsPixmapItem *chance2 = new QGraphicsPixmapItem(pic_chance.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_upper);
    chance2->setPos(FIELD_HEIGHT + FIELD_WIDTH, COLOR_FIELD);

    QGraphicsPixmapItem *chance3 = new QGraphicsPixmapItem(pic_chance.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_right);
    chance3->setRotation(90);
    chance3->setPos(2 * FIELD_HEIGHT + INNER_BOX - COLOR_FIELD, FIELD_HEIGHT + 5 * FIELD_WIDTH);

    QPixmap pic_community_chest(":/images/community_chest.png");
    QGraphicsPixmapItem *chest1 = new QGraphicsPixmapItem(pic_community_chest.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_bottom);
    chest1->setPos(INNER_BOX + FIELD_HEIGHT - 2 * FIELD_WIDTH, INNER_BOX + FIELD_HEIGHT + COLOR_FIELD);

    QGraphicsPixmapItem *chest2 = new QGraphicsPixmapItem(pic_community_chest.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_left);
    chest2->setRotation(-90);
    chest2->setPos(COLOR_FIELD, 3 * FIELD_WIDTH + FIELD_HEIGHT);

    QGraphicsPixmapItem *chest3 = new QGraphicsPixmapItem(pic_community_chest.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_right);
    chest3->setRotation(90);
    chest3->setPos(2 * FIELD_HEIGHT + INNER_BOX - COLOR_FIELD, FIELD_HEIGHT + 2 * FIELD_WIDTH);

    QPixmap pic_tax(":/images/tax.png");
    QGraphicsPixmapItem *tax1 = new QGraphicsPixmapItem(pic_tax.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_right);
    tax1->setRotation(90);
    tax1->setPos(2 * FIELD_HEIGHT + INNER_BOX - COLOR_FIELD, FIELD_HEIGHT + 7 * FIELD_WIDTH);

    QGraphicsPixmapItem *tax2 = new QGraphicsPixmapItem(pic_tax.scaled(FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD), rect_bottom);
    tax2->setPos(INNER_BOX + FIELD_HEIGHT - 4 * FIELD_WIDTH, INNER_BOX + FIELD_HEIGHT + COLOR_FIELD);

    QPixmap pic_bulb(":/images/bulb.png");
    QGraphicsPixmapItem *bulb = new QGraphicsPixmapItem(pic_bulb.scaled(FIELD_WIDTH, FIELD_HEIGHT - 2 * COLOR_FIELD), rect_left);
    bulb->setRotation(-90);
    bulb->setPos(COLOR_FIELD, 8 * FIELD_WIDTH + FIELD_HEIGHT);

    QPixmap pic_water(":/images/water.png");
    QGraphicsPixmapItem *water = new QGraphicsPixmapItem(pic_water.scaled(FIELD_WIDTH, FIELD_HEIGHT - 2.5 * COLOR_FIELD), rect_upper);
    water->setPos(FIELD_HEIGHT + 7 * FIELD_WIDTH, 1.5 * COLOR_FIELD);

    QPixmap pic_plane(":/images/plane.png");
    QGraphicsPixmapItem *plane = new QGraphicsPixmapItem(pic_plane.scaled(FIELD_WIDTH, FIELD_HEIGHT - 2 * COLOR_FIELD), rect_right);
    plane->setRotation(90);
    plane->setPos(2 * FIELD_HEIGHT + INNER_BOX - 1.5 * COLOR_FIELD, FIELD_HEIGHT + 4 * FIELD_WIDTH);

    QPixmap pic_train(":/images/train.png");
    QGraphicsPixmapItem *train1 = new QGraphicsPixmapItem(pic_train.scaled(FIELD_WIDTH / 1.5, FIELD_HEIGHT - 2 * COLOR_FIELD), rect_bottom);
    train1->setPos(INNER_BOX + FIELD_HEIGHT - 5 * FIELD_WIDTH + 15, INNER_BOX + FIELD_HEIGHT + 1.7 * COLOR_FIELD);

    QGraphicsPixmapItem *train2 = new QGraphicsPixmapItem(pic_train.scaled(FIELD_WIDTH / 1.5, FIELD_HEIGHT - 2 * COLOR_FIELD), rect_left);
    train2->setRotation(-90);
    train2->setPos(1.7 * COLOR_FIELD, 5 * FIELD_WIDTH + FIELD_HEIGHT - 15);

    QGraphicsPixmapItem *train3 = new QGraphicsPixmapItem(pic_train.scaled(FIELD_WIDTH / 1.5, FIELD_HEIGHT - 2 * COLOR_FIELD), rect_upper);
    train3->setPos( FIELD_HEIGHT + 4 * FIELD_WIDTH + 15, 1.5 * COLOR_FIELD);
}

void Board::add_buttons(QGraphicsItem *rect_outer)
{

    qint64 x_pos = 0;
    qint64 y_pos = 0;
    qint64 rotation = 0;
    qint64 mul = 1;

    for (unsigned i = 0; i < 40; i++)
    {
        if (i % 10 == 0)
            continue;

        if (i > 0 && i < 10)
        {
            x_pos = INNER_BOX + FIELD_HEIGHT - mul * FIELD_WIDTH;
            y_pos = INNER_BOX + FIELD_HEIGHT;
            mul++;
        }

        if (i > 10 && i < 20)
        {
            if (i == 11)
                mul = 9;

            x_pos = 0;
            y_pos = mul * FIELD_WIDTH + FIELD_HEIGHT;
            rotation = -90;
            mul--;
        }

        if (i > 20 && i < 30)
        {
            if (i == 21)
                mul = 0;

            x_pos = FIELD_HEIGHT + mul * FIELD_WIDTH;
            y_pos = 0;
            rotation = 0;
            mul++;
        }

        if (i > 30 && i < 40)
        {
            if (i == 31)
                mul = 0;

            x_pos = 2 * FIELD_HEIGHT + INNER_BOX;
            y_pos = FIELD_HEIGHT + mul * FIELD_WIDTH;
            rotation = 90;
            mul++;
        }

        auto field_at_position_i=fields_[i];
        Field_kind kind = fields_[i]->get_kind();
        Card_Property *card_prop = new Card_Property(field_at_position_i->get_name(),field_at_position_i->get_color(),field_at_position_i->get_rent(),field_at_position_i->get_rent_one_house(),
                                                      field_at_position_i->get_rent_two_houses(),field_at_position_i->get_rent_three_houses(),field_at_position_i->get_rent_four_houses(),
                                                      field_at_position_i->get_rent_hotel(),field_at_position_i->get_house_price(),field_at_position_i->get_hotel_price(),
                                                      field_at_position_i->get_mortgage(),field_at_position_i->get_kind());
        if (kind == Field_kind::PROPERTY || kind == Field_kind::SUPPLY || kind == Field_kind::RAILWAY){
            buttons_fields_.push_back(new Button(fields_[i], fields_[i]->get_color(), rotation, x_pos, y_pos, card_prop,rect_outer));

        }
        else if(kind == Field_kind::CHANCE || kind == Field_kind::COMMUNITY || kind == Field_kind::TAX ){
           buttons_fields_.push_back(new Button(i, fields_[i]->get_name(), kind, rotation, x_pos, y_pos, rect_outer));

        }

    }
}

void Board::animate_dice(){

    set_dice_numbers();


    QString pic_name1=":/images/";
    pic_name1+=QString::number(dice1_);
    pic_name1+=".png";

    QPixmap dice1_pixmap(pic_name1);
    My_pixmap_graphics_item *dice1 = new My_pixmap_graphics_item(dice1_pixmap.scaled(100,150));

    dice1->setOffset(400,-10);

    Animate_Pixmap* animator = new Animate_Pixmap(dice1);
    animator->animaton()->setDuration(2500);
    animator->animaton()->setKeyValueAt(0, QPointF(400,-10));
    animator->animaton()->setKeyValueAt(0.2, QPointF(400, 440));
    animator->animaton()->setKeyValueAt(0.3, QPointF(400, 350));
    animator->animaton()->setKeyValueAt(0.5, QPointF(400, 440));
    animator->animaton()->setKeyValueAt(0.6, QPointF(400, 400));
    animator->animaton()->setKeyValueAt(0.7, QPointF(400, 440));
    animator->animaton()->setKeyValueAt(1, QPointF(400, 440));
    scene_->addItem(dice1);
    animator->animaton()->start();

    QObject::connect(animator->animaton(), SIGNAL(finished()), dice1, SLOT(hide()));
    QString pic_name2=":/images/";
    pic_name2+=QString::number(dice2_);
    pic_name2+=".png";
    QPixmap dice2_pixmap(pic_name2);
    My_pixmap_graphics_item *dice2= new My_pixmap_graphics_item(dice2_pixmap.scaled(100,150));
    dice2->setOffset(530,-10);
    scene_->addItem(dice2);
    Animate_Pixmap  *animator2 = new Animate_Pixmap(dice2);
    animator2->animaton()->setDuration(2500);
    animator2->animaton()->setKeyValueAt(0, QPointF(530,-10));
    animator2->animaton()->setKeyValueAt(0.2, QPointF(530, 440));
    animator2->animaton()->setKeyValueAt(0.3, QPointF(530, 350));
    animator2->animaton()->setKeyValueAt(0.5, QPointF(530, 440));
    animator2->animaton()->setKeyValueAt(0.6, QPointF(530, 400));
    animator2->animaton()->setKeyValueAt(0.7, QPointF(530, 440));
    animator2->animaton()->setKeyValueAt(1, QPointF(530, 440));
    animator2->animaton()->start();
    QObject::connect(animator2->animaton(), SIGNAL(finished()), dice2, SLOT(hide()));
    QObject::connect(animator2->animaton(), SIGNAL(finished()), this, SIGNAL(dice_animation_finished()));

    emit dice_animation_finished();
}

void Board::owned_property_card(QString name)
{
    auto button = get_button_by_name(name);
    auto card=button->get_card_prop();

    if(button->get_show_or_hide()){
     emit show_property_card(card);
    QObject::connect(button, SIGNAL(show_property_card(Card_Property*)),
                     this, SLOT(show_property_card(Card_Property*)));
    button->set_show_or_hide(false);

    }
    else{
         emit hide_property_card(card);
        QObject::connect(button, SIGNAL(hide_property_card(Card_Property*)),
                         this, SLOT(hide_property_card(Card_Property*)));
        button->set_show_or_hide(true);
    }


}
