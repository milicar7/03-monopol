#include "../include/button.hpp"
Button::Button(Field *field, QColor style, qint64 rotation, qint64 x_pos, qint64 y_pos,Card_Property *card_prop, QGraphicsItem *parent)
    : field_(field), o_(field->get_owner()), id_(field->get_id()), style_(style), rotation_(rotation), x_pos_(x_pos), y_pos_(y_pos),card_prop_(card_prop)
{
    text_ = field->get_name();
    price_ = field->get_price();
    rent_ = field->get_rent();
    kind_ = field->get_kind();
    this->setRotation(rotation_);
    this->setPos(x_pos_, y_pos_);
    this->setAcceptHoverEvents(true);
    this->setAcceptedMouseButtons(Qt::LeftButton);
    this->setParentItem(parent);
}


Button::Button(unsigned id, QString text, Field_kind kind, qint64 rotation, qint64 x_pos, qint64 y_pos, QGraphicsItem *parent)
    : id_(id), text_(text), kind_(kind), rotation_(rotation), x_pos_(x_pos), y_pos_(y_pos)
{
    this->setRotation(rotation_);
    this->setPos(x_pos_, y_pos_);
    this->setParentItem(parent);
}

Button::~Button(){}

QRectF Button::boundingRect() const
{
    if (get_rotation() != 0)
        return QRectF(0, 0, FIELD_HEIGHT, FIELD_WIDTH);
    else
        return QRectF(0, 0, FIELD_WIDTH, FIELD_HEIGHT);
}

QPainterPath Button::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void Button::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    //so that letters and rectangles are black
    painter->setPen(QColor(0, 0, 0));

    Field_kind kind = get_kind();

    if (kind == Field_kind::PROPERTY)
    {
        painter->drawRect(0, COLOR_FIELD, FIELD_WIDTH, FIELD_HEIGHT - COLOR_FIELD);

        QFont font("Helvetica");
        font.setPointSize(10);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->drawText(QRectF(0, COLOR_FIELD, FIELD_WIDTH, FIELD_HEIGHT - 2 * COLOR_FIELD), getText(), Qt::AlignHCenter | Qt::AlignTop);

        painter->setBrush(getStyle());
        painter->drawRect(0, 0, FIELD_WIDTH, COLOR_FIELD);

        painter->drawText(QRectF(0, FIELD_HEIGHT - COLOR_FIELD, FIELD_WIDTH, COLOR_FIELD),  QString::number(get_price()) + "€", Qt::AlignCenter | Qt::AlignVCenter);
    }
    else if (kind == Field_kind::RAILWAY || kind == Field_kind::SUPPLY)
    {

        painter->drawRect(0, 0, FIELD_WIDTH, FIELD_HEIGHT);

        QFont font("Helvetica");
        font.setPointSize(8);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->drawText(QRectF(0, 0, FIELD_WIDTH, 3 * COLOR_FIELD), getText(), Qt::AlignHCenter | Qt::AlignTop);

        painter->drawText(QRectF(0, FIELD_HEIGHT - COLOR_FIELD, FIELD_WIDTH, COLOR_FIELD),  QString::number(get_price()) + "€", Qt::AlignCenter | Qt::AlignVCenter);
    }
    else
    {
        painter->drawRect(0, 0, FIELD_WIDTH, FIELD_HEIGHT);

        QFont font("Helvetica");
        font.setPointSize(10);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->drawText(QRectF(0, 0, FIELD_WIDTH, COLOR_FIELD), getText(), Qt::AlignHCenter | Qt::AlignTop);
    }
}

void Button::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event)

    QString player;

    rent_ = field_->get_rent();
    o_ = field_->get_owner();

    if (Owner::BANK == get_owner())
        player = "Bank";
    else if (Owner::BOT1 == get_owner())
        player = "Bot1";
    else if (Owner::BOT2 == get_owner())
        player = "Bot2";
    else if (Owner::BOT3 == get_owner())
        player = "Bot3";
    else
        player = "You";

    QString tooltip = "Owner: " + player + (Owner::BANK == get_owner() ? "" :
                                           "\nRent: " + QString::number(get_rent()) + "€" +
                                            (Field_kind::PROPERTY != field_->get_kind()? "" :
                                             (field_->get_has_hotel()  ? "\nHotel" :
                                                                      "\nNumber of houses: " + QString::number(field_->get_num_of_houses()) ))
                                           );

    this->setToolTip(tooltip);
}
void Button::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
     Q_UNUSED(event)
    if(kind_==Field_kind::PROPERTY || kind_==Field_kind::SUPPLY || kind_==Field_kind::RAILWAY){
    if(!show_or_hide){
     emit hide_property_card(card_prop_);
        show_or_hide=true;
        }
    }
}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
     Q_UNUSED(event)
     std::cout << "Clicked on button";

     if(kind_==Field_kind::PROPERTY || kind_==Field_kind::SUPPLY || kind_==Field_kind::RAILWAY){

         if(show_or_hide){
            emit show_property_card(card_prop_);
            show_or_hide=false;
         }
     }

}

