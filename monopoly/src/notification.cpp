#include "../include/notification.hpp"

Notification::Notification(int x, int y,const QString msg) : msg_box(msg), x_(x),y_(y){

    notif_color = QColor(230, 230, 230);
    bound_color = QColor(Qt::black);

}
void Notification::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*) {
    Q_UNUSED(option)

    QPen pen = painter->pen();
    pen.setWidth(pen_width);

    pen.setColor(bound_color);
    painter->setPen(pen);
    painter->setOpacity(0.7);
    painter->drawRoundedRect(bounding, padding, padding);

    QPainterPath path;
    path.addRoundedRect(bounding, 10, 10);
    painter->fillPath(path, notif_color);
    painter->drawPath(path);

    QFont font("Helvetica", 15);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 0.5);
    pen.setColor(Qt::black);
    painter->setPen(pen);
    painter->setOpacity(1);
    painter->setFont(font);
    painter->drawText(QRectF(x_, y_, width,height),msg_box, Qt::AlignHCenter | Qt::AlignCenter);

}
