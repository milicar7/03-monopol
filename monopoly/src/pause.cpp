#include "../include/pause.h"
#include "../build/ui_pause.h"

Pause::Pause(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Pause)
{
    ui->setupUi(this);

    setWindowFlags(Qt::CustomizeWindowHint);

//    button_click = new QMediaPlayer();
//    button_click->setMedia(QUrl("qrc:/sounds/button_click.wav"));

    connect(ui->resume_button, &QPushButton::clicked, this, &Pause::button_resume_clicked);
    connect(ui->quit_button, &QPushButton::clicked, this, &Pause::button_quit_clicked);
}

Pause::~Pause() {delete ui;}

void Pause::button_click_sound()
{
    if (button_click->state() == QMediaPlayer::PlayingState)
        button_click->setPosition(0);
    else
        button_click->play();
}

void Pause::button_resume_clicked()
{
//    button_click_sound();
    delete this;
}

void Pause::button_quit_clicked()
{
//    button_click_sound();
    QApplication::closeAllWindows();
}
