#include "../include/card_chance_community.hpp"

Card_Chance_Community::Card_Chance_Community(const QString &content,const Card_category &category,std::vector<Card_type> &types)
        : card_content_(content), card_category_(category), card_types_(types) {}

Card_Chance_Community::~Card_Chance_Community(){}


QString Card_Chance_Community::Type() const
{
    QString out="[";
    for (Card_type t : card_types_){
        switch (t)
        {
        case Card_type::Get:
            out +=" Get";
            break;
        case Card_type::Pay:
            out +=" Pay";
            break;
        case Card_type::Jail:
            out +=" Jail";
            break;
        case Card_type::Move:
            out +=" Move";
            break;
        case Card_type::Jail_free:
            out +=" Jail_free";
            break;
        }
    }

    out+=" ]";
    return out;
 }
QString Card_Chance_Community::Category() const
{
     switch (card_category_)
     {
         case Card_category::Chance:
             return "Chance";
         case Card_category::Community:
             return "Community";
     }
     return "This category doesn't exist";
}

QRectF Card_Chance_Community::boundingRect() const {return QRectF(350, 450, 300,100);}

void Card_Chance_Community::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    bool chance=false;
    if (getCategory()==Card_category::Chance)
        chance=true;
    if (chance)
        painter->setBrush(QColor( 255, 133, 51));
    else
        painter->setBrush(QColor(102, 224, 255));
    painter->drawRect(350,450,300,100);
    QFont font("Helvetica");
    font.setPointSize(10);
    font.setWeight(QFont::Bold);
    painter->setFont(font);
    QString text_to_display;
    if(chance)
        text_to_display="CHANCE\n\n";
    else
        text_to_display="COMMUNITY CHEST\n\n";
    text_to_display+=getContent();
    painter->drawText(QRectF(350, 450, 300,100),text_to_display, Qt::AlignHCenter|Qt::AlignCenter);
}

Card_category Card_Chance_Community::getCategory() const {return card_category_;}

QString Card_Chance_Community::getContent() const {return card_content_;}

void Card_Chance_Community::show(std::ostream &out) const
{
    out<<"Category " << Category().toStdString()  << "\n" << "Content:" << card_content_.toStdString()<<std::endl;
    out << Type().toStdString() << std::endl;

}
std::ostream &operator<<(std::ostream &out, const Card_Chance_Community &cardcc)
{
    cardcc.show(out);
    return out;
}
std::vector<Card_type> Card_Chance_Community::get_card_types() const {return card_types_;}


