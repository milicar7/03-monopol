#include "../include/card_property.hpp"

Card_Property::Card_Property(QString name, QColor color, int rent_lot, int rent_one_house, int rent_two_houses, int rent_three_houses,
                             int rent_four_houses, int rent_hotel, int house_price, int hotel_price, int mortgage, Field_kind kind)
                         : name_(name), color_(color), rent_lot_(rent_lot), rent_one_house_(rent_one_house), rent_two_houses_(rent_two_houses),
                           rent_three_houses_(rent_three_houses), rent_four_houses_(rent_four_houses), rent_hotel_(rent_hotel),
                           house_price_(house_price), hotel_price_(hotel_price), mortgage_(mortgage), kind_(kind) {}

QRectF Card_Property::boundingRect() const {return QRectF(350, 350, 300,300);}

void Card_Property::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    Q_UNUSED(option)
    Q_UNUSED(widget)
    if (kind_ == Field_kind::PROPERTY)
    {
        painter->drawRect(350, 350, 300,300);
        painter->setBrush(color_);
        painter->drawRect(350, 350, 300, 50);

        QFont font("Helvetica");
        font.setPointSize(12);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->setPen(Qt::white);
        painter->drawText(QRectF(350, 350, 300, 50), name_, Qt::AlignCenter | Qt::AlignCenter);

        painter->setPen(Qt::black);
        painter->setBrush(Qt::white);
        painter->drawRect(350, 400, 300, 250);



        font.setPointSize(11);
        painter->setFont(font);

        painter->drawText(QRectF(370, 410, 260,20),"Rent", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 410, 260,20),QString::number(rent_lot_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 430, 260,20),"Rent with one house", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 430, 260,20),QString::number(rent_one_house_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 450, 260,20),"Rent with two houses", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 450, 260,20),QString::number(rent_two_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);


        painter->drawText(QRectF(370, 470, 260,20),"Rent with three houses", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 470, 260,20),QString::number(rent_three_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);


        painter->drawText(QRectF(370, 490, 260,20),"Rent with four houses", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 490, 260,20),QString::number(rent_four_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 510, 260,20),"Rent with HOTEL", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 510, 260,20),QString::number(rent_hotel_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawLine(350,540,650,540);
        painter->drawText(QRectF(370, 550, 260,20),"Houses cost", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 550, 260,20),QString::number(house_price_)+"€ each", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 570, 260,20),"Hotels cost", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 570, 260,20),QString::number(hotel_price_)+"€ each", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 600, 260,20),"Mortgage value", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 600, 260,20),QString::number(mortgage_)+"€", Qt::AlignRight | Qt::AlignCenter);
    }

    if (kind_ == Field_kind::RAILWAY)
    {
        QPixmap pic_on_card;
        painter->setBrush(Qt::white);
        painter->drawRect(350, 350, 300,300);
        if(name_.contains("aerodrom", Qt::CaseInsensitive)){
               pic_on_card.load(":/images/plane.png");
        }
        else {

              pic_on_card.load(":/images/train.png");
        }
        painter->drawPixmap(450,350,80,80,pic_on_card);
        QFont font("Helvetica");
        font.setPointSize(12);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->setPen(Qt::black);
        painter->drawText(QRectF(350, 430, 300, 50), name_, Qt::AlignCenter | Qt::AlignCenter);

        font.setPointSize(11);
        painter->setFont(font);

        painter->drawText(QRectF(370, 480, 260,30),"RENT", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 480, 260,30),QString::number(rent_lot_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 510, 260,30),"If 2 stations are owned", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 510, 260,30),QString::number(rent_two_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 540, 260,30),"If 3 stations are owned", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 540, 260,30),QString::number(rent_three_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 570, 260,30),"If 4 stations are owned", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 570, 260,30),QString::number(rent_four_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 610, 260,30),"MORTGAGE VALUE", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 610, 260,30),QString::number(mortgage_)+"€", Qt::AlignRight | Qt::AlignCenter);

    }
    if (kind_ == Field_kind::SUPPLY)
    {
        QPixmap pic_on_card;
        painter->setBrush(Qt::white);
        painter->drawRect(350, 350, 300,300);

        if(name_.contains("EPS", Qt::CaseInsensitive)){
               pic_on_card.load(":/images/bulb.png");
        }
        else {

              pic_on_card.load(":/images/water.png");
        }
        painter->drawPixmap(460,350,80,80,pic_on_card);
        QFont font("Helvetica");
        font.setPointSize(12);
        font.setWeight(QFont::Bold);

        painter->setFont(font);
        painter->setPen(Qt::black);
        painter->drawText(QRectF(350, 430, 300, 50), name_, Qt::AlignCenter | Qt::AlignCenter);

        font.setPointSize(11);
        painter->setFont(font);

        painter->drawText(QRectF(370, 480, 260,60),"RENT", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 480, 260,60),QString::number(rent_lot_)+"€", Qt::AlignRight | Qt::AlignCenter);

        painter->drawText(QRectF(370, 540, 260,60),"If both supplies are owned", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 540, 260,60),QString::number(rent_two_houses_)+"€", Qt::AlignRight | Qt::AlignCenter);


        painter->drawText(QRectF(370, 610, 260,30),"MORTGAGE VALUE", Qt::AlignLeft | Qt::AlignVCenter);
        painter->drawText(QRectF(370, 610, 260,30),QString::number(mortgage_)+"€", Qt::AlignRight | Qt::AlignCenter);

    }


}
