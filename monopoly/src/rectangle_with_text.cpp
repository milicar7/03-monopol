#include "../include/rectangle_with_text.hpp"

Rectangle_with_text::Rectangle_with_text(QString text, QColor color, int x_pos, int y_pos, int width, int height,
                                         Button_kind kind, int id_for_drawing, QGraphicsItem *parent)
    : text_(text), color_(color), x_pos_(x_pos),y_pos_(y_pos),width_(width),height_(height), kind_(kind), id_for_drawing_(id_for_drawing)
{
 if (text_=="End turn")
     this->blockSignals(true);
}


Rectangle_with_text::Rectangle_with_text(QString text, QColor color, Button_kind kind, QGraphicsItem *parent)
    : text_(text), color_(color),  kind_(kind)
{}


void Rectangle_with_text::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
   Q_UNUSED(option)
   Q_UNUSED(widget)
    if(kind_==Button_kind::BOUGHT_PROPERTY){


        painter->setPen(Qt::white);
    }
    else{
        painter->setPen(Qt::black);
    }
   painter->setBrush(color_);
   painter->drawRect(x_pos_,y_pos_,width_,height_);
   QFont font("Helvetica");
   font.setPointSize(10);
   font.setWeight(QFont::Bold);
   painter->setFont(font);
   painter->drawText(QRectF(x_pos_, y_pos_, width_,height_),text_, Qt::AlignHCenter | Qt::AlignCenter);

}

void Rectangle_with_text::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    if(kind_==Button_kind::BOUGHT_PROPERTY){
          emit show_card_for_the_owned_property(text_);

   }
   else{ //kind_==Button_kind::INTERFACE_BUTTON
        if (text_=="Roll the dice!"){
            std::cout << "Clicked on roll the dice"<<std::endl;

            emit roll_dice();
            this->blockSignals(true);

        }
        else if (text_=="I want to buy it"){
            std::cout << "Clicked on I want to buy it"<<std::endl;

            emit i_want_to_buy_it();
        }

        else if(text_ == "I don't want to buy it")
        {
            std::cout << "Clicked on I don't want to buy it"<<std::endl;
            emit i_dont_want_to_buy_it();
        }
        else if (text_ == "200€"){
            std::cout << "Clicked on 200€"<<std::endl;
            emit tax_option_one();
        }
        else if (text_ == "10% of your total worth"){
            std::cout << "Clicked on 10% of your total worth"<<std::endl;
            emit tax_option_two();
        }
        else if(text_ == "No, I want to pay 50€")
        {
            std::cout << "Clicked on I want to pay to get out of jail"<<std::endl;
            emit i_want_to_pay_for_jail();
        }
        else if(text_ == "Yes, I want to use the card")
        {
            std::cout << "Clicked on I want to use the card"<<std::endl;
            emit i_want_to_use_the_card();
        }
        else if(text_ == "I don't want to mortgage property"){
            std::cout << "Clicked on Im not mortgaging"<<std::endl;
            emit im_not_mortgaging();
        }
        else if(text_ == "Okay"){
            std::cout<<"kliknuto Okay dugme a pre bilo samo za I dont have money to buy"<<std::endl;
            emit okay_i_dont_have_money_to_buy();
        }
        else if(text_ == "I want to lift the mortgage")
        {
            std::cout<<"I want to lift the mortgage"<<std::endl;
            emit i_want_to_lift_the_mortgage();
        }
        else if(text_ == "I don't want to lift the mortgage")
        {
            std::cout<<"I don't want to lift the mortgage"<<std::endl;
            emit i_dont_want_to_lift_the_mortgage();
        }
        else if(text_ == "sell this property?")
        {
            std::cout<<"sell this property?"<<std::endl;
            emit i_want_to_sell_property();
        }
        else if(text_ == "mortgage this property?")
        {
            std::cout<<"mortgage this property?"<<std::endl;
            emit i_want_to_mortgage_this_property();
        }
        else if(text_ == "do nothing?")
        {
            std::cout<< "do nothing?"<<std::endl;
            emit i_want_to_do_nothing();
        }
        else if(text_ == "sell estate?")
        {
            std::cout<<"sell estate?"<<std::endl;
            emit i_want_to_sell_estate();
        }
        else if(text_ == "build a house here?")
        {
            std::cout<<"build house here?"<<std::endl;
            emit i_want_to_build_house();
        }
        else if(text_ == "build a hotel here?")
        {
            std::cout<<"build hotel here?"<<std::endl;
            emit i_want_to_build_hotel();
        }
        else if(text_ == "all estates")
        {
            std::cout<<"I want to sell all estates"<<std::endl;
            emit i_want_to_sell_all_estates();
        }
        else if(text_ == "one estate")
        {
            std::cout<<"I want to sell one estate"<<std::endl;
            emit i_want_to_sell_one_estate();

        }
        else if(text_=="End turn"){
            emit end_turn();
             this->blockSignals(true);
        }
        else {

            std::cout<<"GRESKA"<<std::endl;
        }
    }
}

void Rectangle_with_text::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    if(kind_==Button_kind::BOUGHT_PROPERTY){

             emit hide_card_for_the_owned_property(text_);
    }
}

void Rectangle_with_text::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{ 
    Q_UNUSED(event)
    if(kind_==Button_kind::BOUGHT_PROPERTY){
             if(color_!=Qt::black){
                emit put_property_under_mortgage(text_);
                }
    }
}

void Rectangle_with_text::enable_button(QString text)
{
    if(text_==text){
        this->blockSignals(false);
    }
}
