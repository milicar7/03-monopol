#include "../include/game.hpp"

Game::Game(Board *b,QString name, QString token):board_(b)
{
    Player* human        = new Player(0, name);
    Player* bot1         = new Bot(1, "Bot player 1");
    Player* bot2         = new Bot(2, "Bot player 2");
    Player* bot3         =  new Bot(3, "Bot player 3");

    players.push_back(human);
    players.push_back(bot1);
    players.push_back(bot2);
    players.push_back(bot3);

    curr_field_ = new Field(0, "Start", Field_kind::GO);

    chance_cards = board_->get_chance_cards();
    community_cards = board_->get_community_cards();

    chance_jail_card = std::pair(nullptr, false);
    community_jail_card = std::pair(nullptr, false);
    srand((unsigned) time(0));
    token_human_=token;

    buttons_ = board_->get_buttons();
}

int calculate_adjustment(QString str){
    int adjust = 0;
    if(!str.contains("ship"))
    {
        adjust = 15;
    }
    return adjust;
}

void Game::initialize_board(QGraphicsScene *scene_table)
{
    board_->set_scene(scene_table);

    board_->display_table();
    display_user_info();

    std::vector<QString> tokens;
    tokens.push_back(":/images/duck.png");
    tokens.push_back(":/images/hat.png");
    tokens.push_back(":/images/ship.png");
    tokens.push_back(":/images/car.png");

    std::remove(tokens.begin(), tokens.end(), token_human_);

    QString str = token_human_;
    QPixmap token_pixmap(str);
    QGraphicsPixmapItem *player= new QGraphicsPixmapItem(token_pixmap);
    player->setOffset(900, 910+calculate_adjustment(str));
    scene_table->addItem(player);
    Animate_Player *ap = new Animate_Player(str, player);
    board_->addPlayer(ap);

    str = tokens[0];
    QPixmap token_pixmap1(str);
    QGraphicsPixmapItem *bot1= new QGraphicsPixmapItem(token_pixmap1);
    bot1->setOffset(900, 910+calculate_adjustment(str));
    scene_table->addItem(bot1);
    Animate_Player *ap1 = new Animate_Player(str, bot1);
    board_->addPlayer(ap1);

    str = tokens[1];
    QPixmap token_pixmap2(str);
    QGraphicsPixmapItem *bot2= new QGraphicsPixmapItem(token_pixmap2);
    bot2->setOffset(900, 910+calculate_adjustment(str));
    scene_table->addItem(bot2);
    Animate_Player *ap2 = new Animate_Player(str, bot2);
    board_->addPlayer(ap2);

    str = tokens[2];
    QPixmap token_pixmap4(str);
    QGraphicsPixmapItem *bot3= new QGraphicsPixmapItem(token_pixmap4);
    bot3->setOffset(900, 910+calculate_adjustment(str));
    scene_table->addItem(bot3);
    Animate_Player *ap3 = new Animate_Player(str, bot3);
    board_->addPlayer(ap3);
}

void Game::init_game(){

    for (Player* player: players){

        bank_.give_init_money(*player);
        order.push(player);
    }

   emit next_player();
}

Game::~Game(){

    for (Player* player : players){
        delete player;
    }

    if (community_jail_card.first != nullptr)
        delete community_jail_card.first;


    if (chance_jail_card.first != nullptr)
        delete chance_jail_card.first;

    while(!order.empty()){
        order.pop();
    }
}

void Game::return_score(){
    std::vector<std::pair<QString, int>> score;
    for(auto p:players){
        score.push_back(std::pair(p->get_name(), p->calculate_total_worth()));
    }
    std::sort(score.begin(), score.end(),
              [](std::pair<QString, int> a, std::pair<QString, int> b){return a.second > b.second;});

    Game_over *game_over = new Game_over(score);
    QApplication::closeAllWindows();
    game_over->showFullScreen();
}

Owner Game::id_to_owner(unsigned player_id){
    if (player_id == 0)
        return Owner::HUMAN;
    else if (player_id == 1){
        return Owner::BOT1;
    }
    else if (player_id == 2){
        return Owner::BOT2;
    }
    else if(player_id == 3){
        return Owner::BOT3;
    }
    return Owner::BANK;

}

unsigned Game::owner_to_id(const Owner o){
    if (o == Owner::HUMAN)
        return 0;
    else if (o == Owner::BOT1){
        return 1;
    }
    else if (o == Owner::BOT2){
        return 2;
    }
    else if(o == Owner::BOT3){
        return 3;
    }
    return 4;

}

Player* Game::get_player_by_id(const Owner o){
    unsigned id = owner_to_id(o);

    for (auto player: players){
        if (player->get_id() == id)
            return player;
    }
    return nullptr;
}

void Game::return_jail_card(Field_kind card_from){

    if(card_from == Field_kind::CHANCE){
        chance_cards.push_back(chance_jail_card.first);
    }
    else if( card_from == Field_kind::COMMUNITY){
        community_cards.push_back(community_jail_card.first);
    }
}

void Game::set_jail_card(Field_kind which, bool set, Card_Chance_Community* card){

    if (which == Field_kind::CHANCE){
        chance_jail_card.second = set;
        chance_jail_card.first = card;
    }
    else{
        community_jail_card.second = set;
        community_jail_card.first = card;
    }
}

inline int Game::calculate_mortgage(int mortgage) const
{
    return mortgage * 1.1;
}

void Game::mortgage_property(Field& field, Player& player){

    if(field.is_mortgaged()){
        notification_string_.append(player.get_name() + " can't mortgage this field because it is already mortgaged. ");
        return;
    }

    auto clr_group = player.return_color_group(field.get_color());

    if ((clr_group.size() == 3 && field.get_color() != "mediumpurple" && field.get_color() != "darkblue")
        || (clr_group.size() == 2 && (field.get_color() == "mediumpurple" || field.get_color() == "darkblue")))
    {
        notification_string_.append(player.get_name() + " sells all estates on all fields from color group of field. ");
        for(auto f:clr_group){
            if(f->has_hotel()){
                f->destroy_hotel();
                player.set_number_of_hotels(false);
                bank_.buy_estate(f->get_hotel_price(), false);
                player.receive_money(f->get_hotel_price());
            }

            for(int i = 0 ;i < f->get_num_of_houses(); i++){
                f->destroy_house();
                player.set_number_of_houses(false);
                bank_.buy_estate(f->get_house_price(), true);
                player.receive_money(f->get_house_price());
            }
        }
    }

    field.set_mortgage(true);
    bank_.loan_money(field.get_mortgage());
    player.receive_money(field.get_mortgage());
    notification_string_.append(player.get_name() + " mortgaged " + field.get_name() + ". ");

    if(curr_field_->get_id() == 30)
    {
        emit finished_with_mortgaging_property(50);
    }
    else if(curr_field_->get_id() == 4 || curr_field_->get_id() == 38)
    {
        emit finished_with_mortgaging_property(curr_player_->get_needed_money());
    }
    else if (chance_community_card_shown_ && curr_player_->get_needed_money()!=0)
    {
        emit finished_with_mortgaging_property(curr_player_->get_needed_money());
    }
    else if(id_to_owner(curr_player_->get_id()) != curr_field_->get_owner())
    {
        emit finished_with_mortgaging_property(curr_field_->get_rent());
    }
}

void Game::be_bankrupt(Player* curr_player){

    notification_string_.append(curr_player->get_name() + " is bankrupt! ");
    curr_player->sell_all_estates();
    auto fields = curr_player->get_fields();
    bank_.add_fields(fields);
    bank_.add_hotels(curr_player->get_number_of_hotels());
    bank_.add_houses(curr_player->get_number_of_houses());

}

void Game::buy_from_bank(Field& curr_field, Player& curr_player){

    if(curr_player.add_property(curr_field)){
        bank_.sell_property(curr_field.get_id());
        auto owner = id_to_owner(curr_player.get_id());
        curr_field.change_owner(owner);

        notification_string_.append(curr_player.get_name() + " bought field " + curr_field.get_name() + ".\n");
        notification_string_.append(curr_player.get_name() + " currently has " + QString::number( curr_player.get_budget()) +"€. ");

    }
    else{
          notification_string_.append(curr_player.get_name() + " couldn't buy " + curr_field.get_name() + ". " );
    }
}

bool Game::lift_mortgage(Field& curr_field, Player* curr_player){
    int mortgage_w_interest = calculate_mortgage(curr_field.get_mortgage());
    auto bot_player =  dynamic_cast<Bot*>(curr_player);
    if(bot_player!=nullptr){
         if(bot_player->decide_to_lift_mortgage(mortgage_w_interest)>0.4){
             if(curr_player->pay_mortgage(mortgage_w_interest)){

                 bank_.collect(mortgage_w_interest);
                 curr_field.set_mortgage(false);
                 notification_string_.append(curr_player->get_name() + " lifted mortgage. ");
                 return true;
             }
             else{

                 notification_string_.append(curr_player->get_name() + " doesn't have enough money to lift the mortgage. ");
                 return false;
             }

         }
         else {

             notification_string_.append(curr_player->get_name() + " decited not to lift the mortgage. ");
             return false;
         }

    }
}

void Game::selling_all_estates(Field &curr_field, Player &curr_player)
{
    auto clr_group = curr_player.return_color_group(curr_field.get_color());
    for (auto f :clr_group){
        bool house = curr_player_->sell_estate(*f, true);
        if(house){
            bank_.buy_estate(f->get_house_price(), house);
        }
        else{
            bank_.buy_estate(f->get_hotel_price(), house);
        }

    }
}

void Game::try_payment_to_bank(Player *curr_player, int money)
{


    if(curr_player->get_id()!=0){
        if(curr_player->pay_to_bank(money)){

            notification_string_.append(curr_player->get_name() + " paid " + QString::number(money) + "€ to Bank. "); // DONE
            notification_string_.append("\nCurrent budget of " + curr_player->get_name() + " is " + QString::number(curr_player->get_budget()) + "€. ");

        }
       else {
             mortgage_to_pay_debt(curr_player,money);
        }
    }
    else{

        if(!curr_player->pay_to_bank(money)){

            QGraphicsTextItem *mortgage_text = new QGraphicsTextItem("You have to mortgage some property because you don't have money to pay."
                                                                          "Please choose a property to mortgage on the"
                                                                          "list of properties you own by doing a double mouse click on it!"
                                                                          "If you don't mortgage any property you'll go bankrupt.");
            mortgage_text->setTextWidth(200);
            mortgage_text->setPos(1640, 575);
            board_->scene_->addItem(mortgage_text);

            Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
            board_->scene_->addItem(not_mortgaging);

            QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

            curr_player->set_needed_money(money);
            curr_player_->disable_all_rectangles_with_text(false);

            QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
            QObject::connect(this, SIGNAL(finished_with_mortgaging_property(int)), this, SLOT(pay_after_mortgaging(int)));
            QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_mortgage_question()));
        }
        else{
            bank_.card_payment(false, money);
            emit change_money_from_a_card_signal();
        }
    }
}

QString Game::regex_match(QString &content, bool letters)
{
    QRegularExpression rx;
    if (letters)
        rx = QRegularExpression("'[a-zA-Z ]+'");
    else
        rx = QRegularExpression("[0-9]+");

    QRegularExpressionMatch match = rx.match(content);

    return match.captured(0).replace(QString("'"), QString(""));
}



void  Game::step_on_chance_community_field(Player *curr_player, Field &curr_field)
{

    Field_kind kind = curr_field.get_kind();

    auto universal_cards = kind == Field_kind::CHANCE ? &chance_cards : &community_cards;

    Card_Chance_Community* card = universal_cards->front();

    emit display_chance_community_card(card);

    chance_community_card_shown_++;

    universal_cards->pop_front();

    auto card_type = card->get_card_types();
    auto size = card_type.size();

    if (card_type[0] == Card_type::Jail)
    {
        notification_string_.append( curr_player->get_name() + " is in jail.");
        universal_cards->push_back(card);
        emit stepped_on_go_to_jail();
    }
    else if (card_type[0] == Card_type::Jail_free)
    {

        curr_player->set_jail_card(kind);
        set_jail_card(kind, true, card);
        notification_string_.append( curr_player->get_name() + " got a card to get of jail for free");
        int i= curr_player->get_id();
        if(i==0){
             emit got_jail_free_card();
        }
        else next_player();
    }
    else if (card_type[0] == Card_type::Move)
    {
        throw_dices_again = false;
        int i= curr_player->get_id();
        int steps = 0;
        auto content = card->getContent();

        if (content.contains("spaces", Qt::CaseInsensitive))
        {
            steps = -regex_match(content, false).toInt();
            advance_player(*curr_player_, steps);
            emit go_back_three_spaces();
        }
        else
        {
            if (size == 2)
            {
                auto name_of_the_field = regex_match(content, true);
                int current_position_of_the_player= curr_field.get_id();
                if (name_of_the_field == "Utility")
                {

                    int d1 = 12 - current_position_of_the_player> 0 ? 12 - current_position_of_the_player : 40 - current_position_of_the_player + 12;
                    int d2 = 28 - current_position_of_the_player> 0 ? 28 - current_position_of_the_player : 40 - current_position_of_the_player + 28;
                    steps  = d1 < d2 ? d1 : d2 ;

                }
                else if (name_of_the_field == "Railroad")
                {
                   int d1 = 5 - current_position_of_the_player > 0 ?  5 - current_position_of_the_player : 40- current_position_of_the_player+ 5  ;
                   int d2 = 15 - current_position_of_the_player > 0 ?  15 - current_position_of_the_player : 40- current_position_of_the_player+ 15  ;
                   int d3 = 25 - current_position_of_the_player > 0 ?  25 - current_position_of_the_player : 40- current_position_of_the_player+ 25  ;
                   int d4 = 35 - current_position_of_the_player > 0 ?  35 - current_position_of_the_player : 40- current_position_of_the_player+ 35  ;
                   steps = d1 < d2 ? d1 : d2 ;
                   steps = steps < d3 ? steps : d3;
                   steps = steps < d4 ? steps : d4;

                }
            }

            else if (size != 2) // Go to some field that is not special
            {
                auto name_of_the_field = regex_match(content, true);
                auto f_id=0;
                if(name_of_the_field=="Go"){
                    f_id=0;
                }
                else{
                f_id = board_->get_field_id(name_of_the_field);
                }
                //position_of_the_destination=f_id-board_->getCurrent_player()->position();
                int curr_pos = curr_field.get_id();

                if (f_id >= 0 && f_id <= 9 && curr_pos >= 0 && curr_pos <= 9)
                    steps = f_id - curr_pos;
                else
                    steps = f_id > curr_pos ? f_id - curr_pos : 40 - curr_pos + f_id;
            }

        board_->getCurrentPlayer(i)->setAdvance(steps);
        advance_player(*curr_player, steps);

         emit move_by_card();

        }
        universal_cards->push_back(card);
    }
    else if (card_type[0] == Card_type::Get)
    {
        auto content = card->getContent();

        int money = regex_match(content, false).toInt();

        notification_string_.append( curr_player->get_name() + " got this card: " + content + ". ");

        bank_.card_payment(true, money);
        curr_player->receive_money(money);

        notification_string_.append("\nCurrent budget of " + curr_player->get_name() + " is " + QString::number(curr_player->get_budget()) + "€. ");

        universal_cards->push_back(card);
        int i= curr_player->get_id();
        if(i==0){
            emit change_money_from_a_card_signal();
        }
        else
            emit next_player();
    }
    else if(card_type[0] == Card_type::Pay)
    {
        auto content = card->getContent();
        notification_string_.append( curr_player->get_name() + " got this card: " + content + ". ");
        QRegularExpression rx("[0-9]+");
        QRegularExpressionMatchIterator m = rx.globalMatch(content);

        if (!m.hasNext())
        {
            std::cout << "Error in data, can't find price." << std::endl;
        }

        QRegularExpressionMatch match = m.next();

        int pay = match.captured(0).toInt();

        if (content.contains("each", Qt::CaseInsensitive) || content.contains("per", Qt::CaseInsensitive)) // for every house or hotel
        {
            if (!m.hasNext())
            {
                std::cout << "Error in data, can't find price." << std::endl;
            }

            match = m.next();
            int pay_hotel = match.captured(0).toInt();

            int money = pay * curr_player->get_number_of_houses() + pay_hotel * curr_player->get_number_of_hotels();

            try_payment_to_bank(curr_player, money);
        }
        else // BANK
        {
            try_payment_to_bank(curr_player, pay);
        }
        universal_cards->push_back(card);
        int i= curr_player->get_id();
        if(i!=0)
             emit next_player();
    }
}

void Game::step_on_tax_field(Player* curr_player){

    auto player_bot =  dynamic_cast<Bot*>(curr_player);
    if(player_bot!=nullptr){
        if(!player_bot->decide_to_pay_tax()){
            notification_string_.append(  curr_player->get_name()+ " chose to pay 200€. " );
            if(curr_player->pay_to_bank(200)){
                bank_.collect(200);
                notification_string_.append("\nCurrent budget of " + curr_player->get_name() + " is " + QString::number(curr_player->get_budget()) + "€. ");
            }
            else{
                mortgage_to_pay_debt(curr_player,200);
            }
        }
        else {

            auto total_worth = curr_player->calculate_total_worth();
            if(total_worth<10)
                total_worth=10;
            total_worth = 0.1*total_worth;
            notification_string_.append( curr_player->get_name() +" chose to pay 10% of his total worth. ");
            if(curr_player->pay_to_bank(total_worth)){
                bank_.collect(total_worth);
                notification_string_.append("\nCurrent budget of " + curr_player->get_name() + " is " + QString::number(curr_player->get_budget()) + "€. ");
            }
            else{
                 mortgage_to_pay_debt(curr_player,total_worth);
            }
        }
    }
}


void Game::step_on_owner_bank(Field& curr_field, Player* curr_player){

    auto player_bot =  dynamic_cast<Bot*>(curr_player);
    if(player_bot!=nullptr){
        if(player_bot->decide_to_buy_property(curr_field)){
                buy_from_bank(curr_field, *curr_player);
            }
        else  notification_string_.append(curr_player->get_name() + " didn't want to buy " + curr_field.get_name() + ". ");
    }
    else{
        buy_from_bank(curr_field, *curr_player);
    }
}

void Game::step_on_owner_other_player(Field& curr_field, Player* curr_player, Owner owner){
    unsigned sum = curr_field.get_rent();

    Player *p = get_player_by_id(owner);

    if (!curr_field.is_mortgaged()){
             if(curr_player->pay_to_player(p, sum)){

                 notification_string_.append(curr_player->get_name() + " paid " + QString::number(curr_field.get_rent()) +  "€ to player " +  p->get_name()+ ".\n");
                 notification_string_.append(curr_player->get_name() + " currently has " + QString::number( curr_player->get_budget()) +"€. ");
                 if(p->get_id()==0){
                     Rectangle_with_text *budget_rect = qgraphicsitem_cast<Rectangle_with_text*>(board_->view_->itemAt(1055, 570));
                     board_->scene_->removeItem(budget_rect);

                     QString s = QString::number(p->get_budget());
                     s.append("€");
                     budget_rect->set_text(s);
                     board_->scene_->addItem(budget_rect);
                  }
             }
            else{
                 mortgage_to_pay_debt(curr_player,sum);
             }
    }
    else {
        notification_string_.append((curr_player->get_name()) + " is not paying anything because property is under mortgage. ");
    }
}

void Game::mortgage_to_pay_debt(Player* curr_player, int const debt){
    curr_player->set_needed_money(debt);
    auto player_bot =  dynamic_cast<Bot*>(curr_player);
    if(player_bot!=nullptr){

        int chosen_id = player_bot->decide_which_property_to_mortgage(debt);

        if(chosen_id!=-1){
            Field* chosen_field = getBoard()->get_field_by_id(chosen_id);
            notification_string_.append(  curr_player->get_name() + " has to put a property under mortgage.");
            mortgage_property(*chosen_field, *curr_player);
            pay_after_mortgaging(debt);
        }
        else{
            curr_player->set_bankrupted(true);
            be_bankrupt(curr_player);
            if(curr_player->get_id()==0)
                emit player_bankrupted();
        }
    }
}
void Game::pay_jail_fee(Player* curr_player){

    if(curr_player->pay_to_bank(50)){
        bank_.collect(50);
          notification_string_.append(curr_player->get_name() +" paid 50€ to get out of jail. ");
    }
    else{
        mortgage_to_pay_debt(curr_player, 50);
    }
}

void Game::in_jail(Player* curr_player){

    if (curr_player->check_jail_card()){
        auto player_bot =  dynamic_cast<Bot*>(curr_player);
        if(player_bot!=nullptr){

            if(!player_bot->decide_pay_fine_or_jail_card()){
                pay_jail_fee(curr_player);
                }

            else{
                notification_string_.append(curr_player->get_name() +" used get out of jail free card. ");
                Field_kind card_from = curr_player->unset_jail_card();
                return_jail_card(card_from);
                set_jail_card(card_from, false, nullptr);
            }
        }
    }
    else{
         pay_jail_fee(curr_player);
    }
    curr_player->set_jail(false);
}

void Game::go_to_jail(Player& curr_player){
    curr_player.set_jail(true);
    unsigned int position = curr_player.get_position();
    int steps =  position <= 10 ? 10 - position : 40 - position + 10;
    curr_player.move_forward(steps);
    int id = curr_player.get_id();
    board_->getCurrentPlayer(id)->setAdvance(steps);
    play_again = false;
}

void Game::selling_property(Field& curr_field, Player& curr_player){
    bool check = curr_player.has_estates(curr_field.get_color());
    if(!check){
        curr_player.sell_property(curr_field);
        notification_string_.append(curr_player.get_name() + " sold property. ");
        bank_.buy_property(curr_field);
    }
    else{
        notification_string_.append(curr_player.get_name() + " tried to sell the property but can't because it has estates on it. ");
    }
}

int Game::advance_player(Player& curr_player, int const steps){
    auto move = curr_player.move_forward(steps);
    bool pass = move.second;
    if (pass){
        notification_string_.append(curr_player.get_name() + " received 200€."); // DONE
        bank_.pay_salary();
        curr_player.receive_money(__SALARY__);

        if(curr_player.get_id()==0){
            change_budget_rect();
        }
    }
    return move.first;
}
