#include "../include/bank.hpp"

void Bank::pay_salary(){
    
    check_budget(__SALARY__);
    budget -= __SALARY__;
    
    return;
}


bool Bank::check_possesion(const unsigned id) const {return properties.find(id) != end(properties);}

void Bank::add_fields(std::vector<Field *> fields)
{
    for (Field* f:fields){
        properties.insert(std::pair(f->get_id(), *f));
    }
}

void Bank::add_houses(const unsigned houses) {house_counter += houses;}

void Bank::add_hotels(const unsigned houses) {hotel_counter += houses;}

void Bank::give_init_money(Player &p){
    budget -= 1500;
    p.receive_money(1500);
    std::cout << "Bank gave 1500e to player: " << p.get_name().toStdString()<<std::endl;
}

void Bank::sell_property(const unsigned id) {properties.erase(id);}

void Bank::buy_property(const Field f){
    properties.insert(std::pair(f.get_id(), f));
    budget -= f.get_price();
}

void Bank::loan_money(const int mortgage){
    
    check_budget(mortgage);
    budget -= mortgage;
}

bool Bank::sell_house(const int price){
    
    if(house_counter < 0){
        std::cout<<"There are no houses available for sale"<<std::endl;
        return false;
    }
    budget += price;

    house_counter--;
    return true;
}

bool Bank::sell_hotel(const int price){
    
    if(house_counter < 0){
        std::cout<<"There are no hotels available for sale"<<std::endl;
        return false;
    }

    budget += price;

    hotel_counter--;
    return true;
}

void Bank::buy_estate(const int price, bool house){

    check_budget(price/2);
    budget -= price/2;
    if(house){
        house_counter++;
        return;
    }
    hotel_counter++;
    
}

void Bank::collect(const int tax) {budget += tax;}

void Bank::card_payment(const bool pays, const int sum){

    if(pays){
        check_budget(sum);
        budget -= sum;
        return;
    }
    budget += sum;
}

int Bank::get_budget() {return budget;}

int Bank::get_house_counter() {return house_counter;}

int Bank::get_hotel_counter() {return hotel_counter;}

void Bank::check_budget(const int sum){
    if(budget < sum){
        budget += 5000;
    }
    
}
