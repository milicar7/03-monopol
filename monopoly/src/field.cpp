#include "../include/field.hpp"

Field::Field(unsigned field_id, QString name, Field_kind type)
    : field_id_(field_id), name_(name), type_(type)
{
}

Field::Field(unsigned field_id, QString name, int price, int mortgage, QString color, Field_kind type,int rent_lot,
             int rent_two_houses, int rent_three_houses, int rent_four_houses)
    : field_id_(field_id), name_(name), price_(price), mortgage_(mortgage), color_(color), rent_lot_(rent_lot),
      rent_two_houses_(rent_two_houses), rent_three_houses_(rent_three_houses), rent_four_houses_(rent_four_houses), type_(type)
{}

Field::Field(unsigned field_id, QString name, int price, int mortgage, QString color, Field_kind type,int rent_lot,int rent_two_houses)
    : field_id_(field_id), name_(name), price_(price), mortgage_(mortgage), color_(color), rent_lot_(rent_lot),
      rent_two_houses_(rent_two_houses), type_(type)
{}

Field::Field(unsigned field_id, QString name, int price, int mortgage, QString color, int rent_lot, int rent_one_house,
        int rent_two_houses, int rent_three_houses, int rent_four_houses, int rent_hotel,
        int house_price, int hotel_price, Field_kind type)

    : field_id_(field_id), name_(name), price_(price), mortgage_(mortgage), color_(color), rent_lot_(rent_lot), rent_one_house_(rent_one_house),
      rent_two_houses_(rent_two_houses), rent_three_houses_(rent_three_houses), rent_four_houses_(rent_four_houses),
      rent_hotel_(rent_hotel), house_price_(house_price), hotel_price_(hotel_price), type_(type)
{
    number_of_houses_ = 0;
    has_hotel_ = false;
}


void Field::build_house()
{
    if (get_num_of_houses() == 4)
        std::cout << "You can't build another house because you already have four." << std::endl;
    else
        number_of_houses_ += 1;
}

void Field::build_hotel()
{
    if (!get_has_hotel() && get_num_of_houses() == 4)
    {
        has_hotel_ = true;
    }
    else if (get_has_hotel())
    {
        std::cout << "You can't build more than one hotel." << std::endl;
    }
    else if (get_num_of_houses() != 4)
    {
        std::cout << "You can't build hotel because you don't have four houses." << std::endl;
    }
}

void Field::show(std::ostream &out) const
{
    out << "ID " << get_id() << std::endl
        << "Property: " << get_name().toStdString() << std::endl
        <<"Price: " << get_price() << std::endl
          <<"Color: " << get_color().toStdString() << std::endl
         << "Mortgage: " << get_mortgage() << std::endl;
}

int Field::get_rent() const
{
    auto  kind = get_kind();

    if (is_mortgaged())
        return 0;

    if(kind==Field_kind::PROPERTY){
        int number_of_houses = get_num_of_houses();

        if (number_of_houses == 0)
            return get_rent_lot();

        else if (number_of_houses == 1)
            return get_rent_one_house();

        else if (number_of_houses == 2)
            return get_rent_two_houses();

        else if (number_of_houses == 3)
            return get_rent_three_houses();

        else if (number_of_houses == 4 && get_has_hotel())
            return get_rent_hotel();

        else
            return get_rent_four_houses();
    }
    else if(kind == Field_kind::RAILWAY || kind == Field_kind::SUPPLY){
        return get_rent_lot();
    }
}

void Field::destroy_house()
{
    if (get_has_hotel())
    {
        std::cout << "You need to first destroy hotel then house." << std::endl;
        return;
    }

    else if (get_num_of_houses() == 0)
    {
        std::cout << "You can't destroy house because you don't have one." << std::endl;
        return;
    }

    number_of_houses_--;
}

void Field::destroy_hotel()
{
    if (!get_has_hotel())
        std::cout << "You can't destroy hotel because you don't have it." << std::endl;

    else
        has_hotel_ = false;
}

std::ostream &operator<<(std::ostream &out, const Field &field)
{
    field.show(out);
    return out;
}
